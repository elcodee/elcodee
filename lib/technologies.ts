export const technologies = [
    {id: 1, text: "ReactJS", link: "https://reactjs.org/", desc: "A JavaScript library for building user interfaces"},
    {id: 2, text: "NextJS", link: "https://nextjs.org/", desc: "A Useful React Framework"},
    {id: 2, text: "Laravel", link: "https://laravel.com/", desc: "The PHP Framework for Web Artisans"},
    {id: 3, text: "NodeJS", link: "https://nodejs.org/en/", desc: "A JavaScript runtime built on Chrome's V8 JavaScript engine"},
    {id: 4, text: "JavaScript", link: "https://www.javascript.com/", desc: "A programming language that conforms to the ECMAScript specification"},
    {id: 5, text: "PHP", link: "https://php.net/", desc: "A general-purpose scripting language especially suited to web development"},
    {id: 6, text: "TailwindCSS", link: "https://tailwindcss.com/", desc: "A utility-first CSS framework for rapidly building custom designs"},
    {id: 7, text: "MySQL", link: "https://www.mysql.com/", desc: "A relational database management system based on SQL – Structured Query Language"},
    {id: 8, text: "Git", link: "https://git-scm.com/", desc: "A free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency"},
    {id: 11, text: "Visual Studio Code", link: "https://code.visualstudio.com/", desc: "A code editor redefined and optimized for building and debugging modern web and cloud applications"},
];
