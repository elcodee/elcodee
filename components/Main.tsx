import {TypeAnimation} from "react-type-animation";
import {IoIosPin} from "react-icons/io";
import {Heading} from "./Heading";
import {Avatar} from "./Avatar";
import {Links} from "./Links";

// Next.js
import Link from "next/link";

export const Main = () => {
    return (
        <>
            <main
                className="flex md:flex-row md:flex-start flex-col-reverse items-start md:items-center w-full justify-between mb-12 mt-5">
                <div className="flex flex-col">
                    <Link href="https://www.google.com/maps/place/Jakarta,+Daerah+Khusus+Ibukota+Jakarta/@-6.2297209,106.6647036,11z/data=!3m1!4b1!4m6!3m5!1s0x2e69f3e945e34b9d:0x5371bf0fdad786a2!8m2!3d-6.2087634!4d106.845599!16zL20vMDQ0cnY?entry=ttu">
                        <a className="w-fit" target="_blank">
                            <div
                                className="flex items-center p-1 text-sm px-3 mb-4 w-fit rounded-full text-white bg-cyan-500">
                                <IoIosPin className="mr-2 text-xl"/>
                                Jakarta, Indonesia
                            </div>
                        </a>
                    </Link>
                    <Heading style={{marginBottom: "0.5rem"}}>
                        eLCodee <span className="wave">👋</span>
                    </Heading>
                    <div className="text-gray-700 flex font-semibold dark:text-gray-100 mb-4">
                        {new Date().getFullYear() - 2001} y/o &mdash;&nbsp;
                        <TypeAnimation
                            sequence={[
                                "Software Engineer",
                                1000,
                                "Web Developer",
                                1000,
                                "Fullstack Dev",
                                1000,
                                "Frontend Dev",
                                1000,
                                "Backend Dev",
                                1000,
                            ]}
                            wrapper="p"
                            cursor={true}
                            repeat={Infinity}
                        />
                    </div>
                    <Links/>
                </div>
                <div className="flex border mb-8 md:mb-0 duration-300 border-teal-100 dark:border-black rounded-full">
                    <Avatar width={122} height={122}/>
                </div>
            </main>
        </>
    );
};
