import { EMAIL_ADDRESS } from "../lib/constants";
import { Heading } from "./Heading";
import { LinkTag } from "./LinkTag";
import { Text } from "./Text";

export const Contact = () => {
  return (
    <section className="flex flex-col">
      <Heading>Contact 💌</Heading>
      <Text>
        Follow me on{" "}
        <LinkTag href="https://instagram.com/elcodeee" target="_blank">
          Instagram
        </LinkTag>,{" "}
        <LinkTag href="https://facebook.com/elcodeee" target="_blank">
          Facebook
        </LinkTag>,{" "}
        <LinkTag href="https://linkedin.com/in/rmdtya" target="_blank">
          Linkedin
        </LinkTag>
        .
      </Text>
      <Text>
        View My Project At{" "}
        <LinkTag href="https://github.com/elcodee" target="_blank">
          Github
        </LinkTag>,{" "}
        <LinkTag href="https://gitlab.com/elcodee" target="_blank">
          Gitlab
        </LinkTag>
        .
      </Text>
      <Text>
        For more serious matters{" "}
        <LinkTag href="https://wa.me/6285156187157">text me on whatsapp</LinkTag>.
      </Text>
      {/* <Text>
        For more serious matters{" "}
        <LinkTag href={`mailto:${EMAIL_ADDRESS}`}>message me on whatsapp</LinkTag>.
      </Text> */}
    </section>
  );
};
