import Head from "next/head";

interface Props {
    title: string;
    description: string;
}

const structuredData = {
    "@context": "https://schema.org/",
    "@type": "Person",
    "name": "eLCodee",
    "image": "/me.jpg",
    "jobTitle": "Software Developer, Web Developer, Fullstack Developer, Frontend Developer, Backend Developer",
    "url": "https://elcodee.com/",
    "sameAs": [
        "https://github.com/elcode",
        "https://gitlab.com/elcodee",
        "https://www.instagram.com/elcodeee"
    ]
}

export const Seo = () => {
    return (
        <Head>
            <title>eLCodee</title>
            <meta charSet={'UTF-8'}/>
            <meta name="title" content="eLCodee"/>
            <meta name="description"
                  content="Hi there, I'm eLCodee &mdash; a full-stack web application developer from Jakarta, Indonesia."/>
            <meta name="robots" content="index, follow"/>
            <meta property="og:type" content="website"/>
            <meta property="og:url" content="https://elcodee.com/"/>
            <meta property="og:title" content="eLCodee"/>
            <meta property="og:description"
                  content="Hi there, I'm eLCodee &mdash; a full-stack web application developer from Jakarta, Indonesia."/>
            <meta property="og:image" content="/me.jpg"/>
            <meta property="twitter:card" content="summary_large_image"/>
            <meta property="twitter:url" content="https://elcodee.com/"/>
            <meta property="twitter:title" content="eLCodee"/>
            <meta property="twitter:description"
                  content="Hi there, I'm eLCodee &mdash; a full-stack web application developer from Jakarta, Indonesia."/>
            <meta property="twitter:image" content="/me.jpg"/>
            <script
                type="application/ld+json"
                dangerouslySetInnerHTML={{__html: JSON.stringify(structuredData)}}
            />
        </Head>
    );
};
