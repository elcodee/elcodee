import { LinkTag } from "../components/LinkTag";
import NotifiationModal from "../components/Modal";
import { Seo } from "../components/Seo";

const Soon = () => {
  return (
    <>
      <Seo />
      <div className="flex flex-col items-center justify-center w-screen h-screen">
        <h1 className="text-4xl font-bold tracking-tight mb-2 dark:text-white">
          &copy; eLCodee
        </h1>
        <NotifiationModal />
      </div>
    </>
  );
};

export default Soon;
